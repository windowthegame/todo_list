class AddAttachmentAvatarToUsers < ActiveRecord::Migration
  def self.up
    change_table :users do |t|
      t.attachment :avatar
      t.integer :contact_number
    end
  end

  def self.down
    drop_attached_file :users, :avatar
    remove_column :contact_number
  end
end
