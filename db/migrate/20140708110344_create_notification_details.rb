class CreateNotificationDetails < ActiveRecord::Migration
  def change
    create_table :notification_details do |t|
      t.integer :user_id
      t.integer :task_id

      t.timestamps
    end
  end
end
