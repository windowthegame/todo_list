class AddPerfectDayToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :perfect_day, :integer, default: 0
    User.all.each do |user|
    	user.perfect_day = user.perfect_day_count
    	user.save(:validate => false)
    end
  end

  def self.down
    remove_column :users, :perfect_day #, :integer
  end
end
