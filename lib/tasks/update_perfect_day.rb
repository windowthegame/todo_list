namespace :todo_list do
  desc "Change Perfect day"
  task :update_perfect_day => :environment do
  	User.all.each do |user|
  		total_task = user.tasks.where("DATE(created_at) = ? and status not in (?)", Time.now.to_date - 1,["pending","reject"]).length
  		completed_task = user.tasks.where("DATE(created_at) = ? and status = ?", Time.now.to_date - 1,"completed").length
  		if total_task == completed_task
  			user.perfect_day = user.perfect_day + 1
  		else
  			user.perfect_day = 0
  		end
  		user.save(:validate => false)
  	end	
  end
end