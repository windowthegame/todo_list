class Friend < ActiveRecord::Base

	##validations
	validates :user_one,:user_two, :presence => true
	validates_uniqueness_of :user_one, scope: :user_two, :message => "can not sent request again"
	validate :check_sender
	##Relationship
	
	belongs_to :sender, class_name: 'User', :foreign_key => "user_one"
	belongs_to :receiver, class_name: 'User', :foreign_key => "user_two"

	##callback
	before_create :add_default_status

	##scopes
	scope :with_status,lambda { |status| where(status: status) }

	## Instance Methods ##
	def add_default_status
		self.status = 'pending'
	end

	def display_errors
	  self.errors.full_messages.join(', ')
  	end

  	def check_sender
		if self.user_one == self.user_two
			errors.add(:user_one, 'can not sent request.')
		end
	end
end
