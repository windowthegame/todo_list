class Task < ActiveRecord::Base
	# Relationship
	belongs_to :user
  belongs_to :owner, :class_name => 'User', :foreign_key => 'owner_id'
  has_and_belongs_to_many :notification_recivers, :class_name => 'User', :join_table => :notification_details

	## Validations
	validates :user_id, :task_name, :presence => true

	## Callback
	before_create :add_default_status_and_point

  ## Scopes
  scope :active_true, -> { where("status = ? and favorite = ?", "active", true) }
  scope :active_false, -> { where("status = ? and favorite = ?", "active", false) }
  scope :completed_true, -> { where("status = ? and favorite = ?", "completed", true) }
  scope :completed_false, -> { where("status = ? and favorite = ?", "completed", false) }
  scope :today_task, -> { where("DATE(created_at) = ?", Time.now.to_date) }
  scope :public_task, -> { where("category = ?","public") }

	## Methods
	def add_default_status_and_point
    if self.user_id == self.owner_id
		  self.status = 'active'
    else
      self.status = 'pending'
    end
    if self.task_type == 'custom'
      self.point = 1
    elsif self.task_type == 'keyword'
      self.point = 2
    end
        
	end

  def new_amount
    self.amount.present? ? self.amount : ""
  end

  def new_tnumber
    self.tnumber.present? ? self.tnumber : ""
  end

  def new_note
    self.note.present? ? self.note : ''
  end

  def display_errors
    self.errors.full_messages.join(', ')
  end

  def is_active?
  	self.status.downcase == "active"
  end

  def is_private?
  	self.category.downcase == "private"
  end

  def is_public?
  	self.category.downcase == "public"
  end

  def is_keyword?
  	self.task_type.downcase == "keyword"
  end

  def is_custom?
  	self.task_type.downcase == "custom"
  end

  def is_pending?
    self.status.downcase == "pending"
  end

  def category2
    self.category.downcase if self.category.present? 
  end

  def weekly_point
    start_date = Time.now.end_of_week - 7.day
    end_date = Time.now.end_of_week
    self.user.tasks.where("updated_at > ? AND updated_at < ? and status not in (?)", start_date, end_date,["pending","reject"]).sum(:point)
  end

  def send_add_task_notification
    if self.user_id != self.owner_id
      self.user.send_notification("#{self.owner.username} has sent you #{self.task_name} #{self.task_type} Task.")
    end
  end

  def send_completed_task_notification
    users = self.notification_recivers
    if users.present?
      users.each do |user|
        user.send_notification("#{self.user.username} has completed #{self.task_name} Task.")
      end
    end
  end
end