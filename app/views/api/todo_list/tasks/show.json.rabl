object @task
attributes :task_name, :status, :point, :id, :task_type, :favorite, :inner_task

node :category do|task|
	 task.category
end

node :amount do |task|
	task.new_amount
end

node :tnumber do |task|
	task.new_tnumber
end

node :note do |task|
	task.new_note
end

node :created_at do |task|
	task.created_at.strftime("%m/%d/%Y")
end
